## Setup ATLAS
   on lxplus: setupATLAS <br />
   on a different machine: <br />
   export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase <br />
   source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh  <br />

## Setup
   lsetup -a testing asetup <br />
   asetup 21.2,AnalysisBase,latest <br />

   mkdir build; cd build/ <br />
   cmake ../source/ <br />
   make <br />
   source ./x86_64-centos7-gcc8-opt/setup.sh <br />
   mkdir ../submitDir <br />
   
## Run the code
   source example.sh <br />
   
Keep attention that if the input xAOD is IDTRACKVALID, then, please set <br />
   —ReadIDTRKVALID=1    <br />
   